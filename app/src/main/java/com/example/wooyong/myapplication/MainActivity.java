package com.example.wooyong.myapplication;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(MainActivity.this, Main2Activity.class);
        int reqCode = 1;
        startActivityForResult(intent, reqCode);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        LocationManager locationManager =
                (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                showNewLocation(location);
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };
        if (ActivityCompat.checkSelfPermission (this,
                Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission (this,
                Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED )
            return;
        if (locationManager.getAllProviders ().contains(LocationManager.NETWORK_PROVIDER ))
            locationManager.requestLocationUpdates (LocationManager.NETWORK_PROVIDER , 0, 0,
                    locationListener);
        if (locationManager.getAllProviders ().contains(LocationManager.GPS_PROVIDER ))
            locationManager.requestLocationUpdates (LocationManager.GPS_PROVIDER, 0, 0,
                    locationListener);
    }

    public void showNewLocation(Location location) {
        // Provider
        EditText txt = (EditText)findViewById(R.id.editText1);
        txt.setText(location.getProvider());
        // Accuracy
        txt = (EditText)findViewById(R.id.editText2);
        if (location.hasAccuracy()) {
            txt.setText(" " + location.getAccuracy());
        } else {
            txt.setText("Unknown");
        }
        // Longitude
        txt = (EditText)findViewById(R.id.editText3);
        txt.setText(" " + location.getLongitude());
        // Latitude
        txt = (EditText)findViewById(R.id.editText4);
        txt.setText(" " + location.getLatitude());
        // Altitude
        txt = (EditText)findViewById(R.id.editText5);
        if (location.hasAltitude()) {
            txt.setText(" " + location.getAltitude());
        } else {
            txt.setText("Unknown");
        }
        // Time
        txt = (EditText)findViewById(R.id.editText6);
        txt.setText(" " + location.getTime());
        // Bearing
        txt = (EditText)findViewById(R.id.editText7);
        if (location.hasBearing()) {
            txt.setText(" " + location.getBearing());
        } else {
            txt.setText("Unknown");
        }
        // Speed
        txt = (EditText)findViewById(R.id.editText8);
        if (location.hasSpeed()) {
            txt.setText(" " + location.getSpeed());
        } else {
            txt.setText("Unknown");
        }
        // Extras
        txt = (EditText)findViewById(R.id.editText9);
        if (location.getExtras() != null) {
            txt.setText(location.getExtras().toString());
        } else {
            txt.setText("None");
        }
    }
}
